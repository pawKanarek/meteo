﻿using System.Threading.Tasks;
using MeteoPortable.Data;
using MeteoPortable.Infrastructure;

namespace MeteoPortable.Services
{
    public class MeasurementsApiService : IMeasurementsApiService
    {
        private readonly IBaseHttpClientService _httpClientService;

        public MeasurementsApiService(IBaseHttpClientService httpClientService)
        {
            _httpClientService = httpClientService;

            _httpClientService.SetApiBaseUrl("http://meteowww2017.azurewebsites.net/api/");
            //_httpClientService.SetApiBaseUrl("http://localhost:61251/api");
        }

        public async Task<ApiResponseWrapper<GetChartDataResponse>> GetLastChartDataAsync(GetLastChartDataArgs args)
        {
            return await _httpClientService.DoGetAsync<GetLastChartDataArgs, GetChartDataResponse>(
                new ApiRequestData<GetLastChartDataArgs>
                {
                    ApiRequestArgs = args,
                    TargetUrl = "GetLastRecordsChart"
                });
        }

        public async Task<ApiResponseWrapper<GetChartDataResponse>> GetWeekyChartDataAsync(GetChartDataArgs args)
        {
            return await _httpClientService.DoGetAsync<GetChartDataArgs, GetChartDataResponse>(
                new ApiRequestData<GetChartDataArgs>
                {
                    ApiRequestArgs = args,
                    TargetUrl = "GetWeeklyChart"
                });
        }

        public async Task<ApiResponseWrapper<GetChartDataResponse>> GetDailyChartDataAsync(GetChartDataArgs args)
        {
            return await _httpClientService.DoGetAsync<GetChartDataArgs, GetChartDataResponse>(
                new ApiRequestData<GetChartDataArgs>
                {
                    ApiRequestArgs = args,
                    TargetUrl = "GetDailyChart"
                });
        }
    }
}
﻿using System.Net.Http;
using System.Threading.Tasks;
using MeteoPortable.Data;
using MeteoPortable.Infrastructure;

namespace MeteoPortable.Services
{
    public interface IBaseHttpClientService
    {
        string ApiBaseUrl { get; }

        Task<ApiResponseWrapper<TOut>> DoGetAsync<TArgs, TOut>(ApiRequestData<TArgs> requestData)
            where TOut : ResponseBase;

        Task<ApiResponseWrapper<TOut>> DoPostAsync<TArgs, TOut>(ApiRequestData<TArgs> requestData)
            where TOut : ResponseBase;

        Task<ApiResponseWrapper<TOut>> DoSendRequestAsync<TOut>(HttpRequestMessage reqMsg) where TOut : ResponseBase;

        void SetApiBaseUrl(string apiBaseUrl);
    }
}
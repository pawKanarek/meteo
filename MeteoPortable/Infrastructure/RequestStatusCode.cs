﻿namespace MeteoPortable.Infrastructure
{
    public enum RequestStatusCode
    {
        #region GeneralErrors

        Success = 0,
        HttpError = 800,
        WebException = 801,
        TimeoutException = 802,
        Cancelled = 803,
        NoConnection = 804,
        InterruptedByUser = 805,

        #endregion GeneralErrors

        #region DroidErrors

        JavaUnknownHost = 850,
        JavaSocketException = 851,
        JavaSocketTimeoutException = 852,

        #endregion DroidErrors

        #region OtherErrors

        UnknownError = 999

        #endregion OtherErrors
    }
}
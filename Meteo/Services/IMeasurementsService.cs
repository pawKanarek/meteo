﻿using System.Threading.Tasks;
using MeteoPortable.Data;

namespace MeteoWww.Services
{
    public interface IMeasurementsService
    {
        Task<GetMeasurementsResponse> GetAllMeasurementsAsync();
        Task<GetMeasurementResponse> GetSingleMeasurementAsync(int? id);
        Task<GetMeasurementsResponse> GetNewestMeasurementsAsync();
        Task<InsertMeasurementsResponse> InsertMeasurementsAsync(Measurement[] measurements);
        Task<GetChartDataResponse> GetLastRecordsAsync(int? itemsToReturn);
        Task<GetChartDataResponse> GetDailyRecordsAsync();
        Task<GetChartDataResponse> GetWeeklyRecordsAsync();
        Task ClearDatabase();
        Task Generate3DaySeries();
        Task GenerateSingleSeries();
    }
}
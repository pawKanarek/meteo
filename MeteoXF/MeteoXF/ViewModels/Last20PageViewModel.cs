﻿using System.Threading.Tasks;
using MeteoPortable.Data;
using MeteoPortable.Services;
using MeteoXF.Services;
using Prism.Navigation;

namespace MeteoXF.ViewModels
{
    public class Last20PageViewModel : ViewModelBase
    {
        private readonly IMeasurementsApiService _measurementsApiService;
        private readonly INavigationService _navigationService;

        public Last20PageViewModel(INavigationService navigationService, IMeasurementsApiService measurementsApiService, ISignalRService signalRService, IAlertService alertService)
            : base(navigationService, signalRService, alertService)
        {
            _navigationService = navigationService;
            _measurementsApiService = measurementsApiService;

            Title = "Ostatnie 20";

            Task.Run(async () => { await FillDataAsync(); });
        }

        public override async Task FillDataAsync()
        {
            IsDataLoading = true;
            IsDataLoaded = false;
            var response = await _measurementsApiService.GetLastChartDataAsync(new GetLastChartDataArgs {Count = 20});
            IsDataLoading = false;

            if (response.ApiResponse.Result.Success)
            {
                IsDataLoaded = true;
                CreateChart(response.ApiResponse.ChartItems);
            }
        }
    }
}
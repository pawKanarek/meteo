﻿using MeteoPortable.Infrastructure;

namespace MeteoPortable.Data
{
    public class GetMeasurementsResponse : ResponseBase
    {
        public Measurement[] Measurements { get; set; }
    }
}
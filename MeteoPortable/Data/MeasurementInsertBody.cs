﻿using System.Collections.Generic;

namespace MeteoPortable.Data
{
    public class MeasurementInsertBody
    {
        public List<Measurement> Measurements { get; set; }
    }
}
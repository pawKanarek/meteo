﻿using System;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Meteo.Models;
using MeteoPortable.Services;
using MeteoWww.Models;
using MeteoWww.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Meteo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public IContainer ApplicationContainer { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddSignalR();

            services.AddAntiforgery(o => o.HeaderName = "XSRF-TOKEN");
            services.AddMvc();
            services.AddKendo();
            services.AddDbContext<MeteoContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("MeteoContext")));

            var builder = new ContainerBuilder();
            builder.Populate(services);

            builder.RegisterType<MeasurementsService>().As<IMeasurementsService>().InstancePerLifetimeScope();
            builder.RegisterType<BaseHttpClientService>().As<IBaseHttpClientService>().InstancePerLifetimeScope();
            builder.RegisterType<OneSignalService>().As<IOneSignalService>().InstancePerLifetimeScope();

            ApplicationContainer = builder.Build();
            return new AutofacServiceProvider(ApplicationContainer);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseKendo(env);

            app.UseStaticFiles();

            app.UseSignalR(routes =>
            {
                routes.MapHub<SignalRHub>("SignalRHub");
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    "default",
                    "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
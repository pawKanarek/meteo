﻿using System.Threading.Tasks;

namespace MeteoXF.Services
{
    public interface IConnectivityService
    {
        bool IsNetworkConnected { get; }
        Task<bool> IsInternetAvailable(string hostToCheck);
    }
}
﻿namespace MeteoPortable.Infrastructure
{
    public enum MeasurementType
    {
        TemperatueInside = 0,
        TemperatureOutside,
        Pressure,
        Humidity,
        Smog
    }
}
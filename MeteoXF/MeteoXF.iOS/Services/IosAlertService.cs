﻿using System;
using System.Threading.Tasks;
using MeteoXF.Services;
using UIKit;

namespace MeteoXF.iOS.Services
{
    public class IosAlertService : IAlertService
    {
        public async Task DisplayAlertAsync(string title, string message, string cancel)
        {
            var tcs = new TaskCompletionSource<bool>();
            UIDevice.CurrentDevice.BeginInvokeOnMainThread(() =>
            {
                var window = InitializeWindow();
                var alertController = GetBaseAlertController(title, message);
                alertController.AddAction(CreateActionWithWindowHide(cancel, UIAlertActionStyle.Cancel,
                    () => { tcs.TrySetResult(true); }, window));

                window.RootViewController.PresentViewController(alertController, true, null);
            });
            await tcs.Task;
        }

        public async Task<bool> DisplayAlertAsync(string title, string message, string ok, string cancel)
        {
            var result = false;
            var tcs = new TaskCompletionSource<bool>();
            UIDevice.CurrentDevice.BeginInvokeOnMainThread(() =>
            {
                var window = InitializeWindow();
                var alertController = GetBaseAlertController(title, message);
                alertController.AddAction(CreateActionWithWindowHide(cancel, UIAlertActionStyle.Cancel,
                    () => { tcs.TrySetResult(false); }, window));
                alertController.AddAction(CreateActionWithWindowHide(ok, UIAlertActionStyle.Default,
                    () => { tcs.TrySetResult(true); }, window));
                window.RootViewController.PresentViewController(alertController, true, null);
            });
            result = await tcs.Task;
            return result;
        }

        private UIAlertController GetBaseAlertController(string title, string message)
        {
            var alertController = UIAlertController.Create(title, message, UIAlertControllerStyle.Alert);
            return alertController;
        }

        private UIAlertAction CreateActionWithWindowHide(string text, UIAlertActionStyle style, Action setResult,
            UIWindow window)
        {
            return UIAlertAction.Create(text, style,
                a =>
                {
                    window.Hidden = true;
                    setResult();
                });
        }

        private UIWindow InitializeWindow()
        {
            var window = new UIWindow {BackgroundColor = UIColor.Clear};
            window.RootViewController = new UIViewController();
            window.RootViewController.View.BackgroundColor = UIColor.Clear;
            window.WindowLevel = UIWindowLevel.Alert + 1;
            window.MakeKeyAndVisible();

            return window;
        }
    }
}
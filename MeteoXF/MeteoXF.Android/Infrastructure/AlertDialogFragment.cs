﻿using Android.App;
using Android.Content;
using Android.OS;
using Xamarin.Forms;

namespace MeteoXF.Droid.Infrastructure
{
    public class AlertDialogFragment : DialogFragment
    {
        public AlertDialogModel AlertDialogModel { get; set; }

        public static AlertDialogFragment NewInstance(Bundle bundle)
        {
            var alertDialogFragment = new AlertDialogFragment();
            alertDialogFragment.Arguments = bundle;
            return alertDialogFragment;
        }

        public override Dialog OnCreateDialog(Bundle savedInstanceState)
        {
            var alertBuilder = new AlertDialog.Builder((Activity) Forms.Context);
            alertBuilder.SetTitle(AlertDialogModel.Title);
            alertBuilder.SetMessage(AlertDialogModel.Message);
            // Simple alert with message and one button
            alertBuilder.SetNegativeButton(AlertDialogModel.NegativeButtonText,
                (sender, args) => { AlertDialogModel.TaskCompletionSource.TrySetResult(false); });

            if (!string.IsNullOrWhiteSpace(AlertDialogModel.PositiveButtonText))
            {
                // Alert with possitive button with input e.g. PIN 
                alertBuilder.SetPositiveButton(AlertDialogModel.PositiveButtonText,
                    (sender, args) => { AlertDialogModel.TaskCompletionSource.TrySetResult(true); });
            }
            var alert = alertBuilder.Create();
            alert.SetCanceledOnTouchOutside(true);
            return alert;
        }

        public override void OnCancel(IDialogInterface dialog)
        {
            AlertDialogModel.TaskCompletionSource.TrySetResult(false);
            base.OnCancel(dialog);
        }
    }
}
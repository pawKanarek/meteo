﻿// <auto-generated />
using MeteoPortable.Infrastructure;
using Meteo.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using System;

namespace Meteo.Migrations
{
    [DbContext(typeof(MeteoContext))]
    [Migration("20180110001301_Initial2")]
    partial class Initial2
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.0-rtm-26452")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("MeteoPortable.Data.Measurement", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTimeOffset>("Created");

                    b.Property<DateTimeOffset>("Recevied");

                    b.Property<string>("Title");

                    b.Property<int>("Type");

                    b.Property<double>("Value");

                    b.HasKey("ID");

                    b.ToTable("Measurements");
                });
#pragma warning restore 612, 618
        }
    }
}

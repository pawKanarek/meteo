﻿using System.Net;
using System.Net.Http.Headers;

namespace MeteoPortable.Infrastructure
{
    public class ApiResponseWrapper<TResult> where TResult : ResponseBase
    {
        public HttpResponseHeaders ResponseHeaders { get; set; }

        public RequestStatusCode StatusCode { get; set; }

        public HttpStatusCode HttpStatusCode { get; set; }

        public string StatusErrorMessage { get; set; }

        public TResult ApiResponse { get; set; }
    }
}
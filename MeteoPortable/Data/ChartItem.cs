﻿using System.Collections.Generic;

namespace MeteoPortable.Data
{
    public class ChartItem
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public List<double[]> Series { get; set; }
        public List<string> SeriesTitle { get; set; }
        public string[] CategoryAxis { get; set; }
    }
}
﻿using MeteoPortable.Infrastructure;

namespace MeteoPortable.Data
{
    public class GetChartDataResponse : ResponseBase
    {
        public ChartItem[] ChartItems { get; set; }
    }
}
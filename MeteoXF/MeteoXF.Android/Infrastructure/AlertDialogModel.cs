﻿using System.Threading.Tasks;

namespace MeteoXF.Droid.Infrastructure
{
    public class AlertDialogModel
    {
        public string Title { get; set; }
        public string Message { get; set; }
        public string PositiveButtonText { get; set; }
        public string NegativeButtonText { get; set; }
        public TaskCompletionSource<bool> TaskCompletionSource { get; set; }
    }
}
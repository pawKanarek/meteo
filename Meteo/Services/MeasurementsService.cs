﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Meteo.Models;
using MeteoPortable.Data;
using MeteoPortable.Infrastructure;
using MeteoWww.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;

namespace MeteoWww.Services
{
    public class MeasurementsService : IMeasurementsService
    {
        private readonly MeteoContext _context;
        private readonly IOneSignalService _oneSignalService;
        private readonly IHubContext<SignalRHub> _signalRHubContext;

        public MeasurementsService(MeteoContext context, IOneSignalService oneSignalService, IHubContext<SignalRHub> signalRHubContext)
        {
            _context = context;
            _oneSignalService = oneSignalService;
            _signalRHubContext = signalRHubContext;

        }

        public async Task<GetMeasurementResponse> GetSingleMeasurementAsync(int? id)
        {
            if (id == null)
            {
                return ServiceResponse<GetMeasurementResponse>.Error().ServiceContractBroken("No id recevied");
            }

            var response = ServiceResponse<GetMeasurementResponse>.Success();

            var measurement = await _context.Measurements.SingleOrDefaultAsync(m => m.ID == id);

            if (measurement == null)
            {
                return ServiceResponse<GetMeasurementResponse>.Error().FieldNotFound("Measurements");
            }

            response.Measurement = measurement;
            return response;
        }

        public async Task<GetMeasurementsResponse> GetAllMeasurementsAsync()
        {
            var response = ServiceResponse<GetMeasurementsResponse>.Success();
            var measurements = await _context.Measurements.ToListAsync();

            if (!measurements.Any())
            {
                return ServiceResponse<GetMeasurementsResponse>.Error().FieldNotFound("Measurements");
            }

            response.Measurements = measurements.ToArray();
            return response;
        }

        public async Task<InsertMeasurementsResponse> InsertMeasurementsAsync(Measurement[] measurements)
        {
            if (!measurements.Any())
            {
                return ServiceResponse<InsertMeasurementsResponse>.Error().FieldNotFound("Measurements");
            }
            await _signalRHubContext.Groups.AddAsync("SignalRHub", "SignalRHub");
            await _context.Measurements.AddRangeAsync(measurements);
            await _context.SaveChangesAsync();
            var highestTemp = measurements.Where(t => t.Type == MeasurementType.TemperatueInside)
                                  .OrderBy(m => m.Value).FirstOrDefault()?.Value ?? 0;
            if (highestTemp >= 40)
            {
                _oneSignalService.SendTemperatureWarning((int) highestTemp);
            }
            return ServiceResponse<InsertMeasurementsResponse>.Success();
        }

        public async Task<GetMeasurementsResponse> GetNewestMeasurementsAsync()
        {
            var measurementsResponse = await GetAllMeasurementsAsync();

            if (!measurementsResponse.Result.Success || !measurementsResponse.Measurements.Any())
            {
                return measurementsResponse;
            }

            var newestDate = measurementsResponse.Measurements.OrderBy(m => m.Created).LastOrDefault()?.Created ??
                             DateTimeOffset.MinValue;
            measurementsResponse.Measurements = measurementsResponse.Measurements
                .Where(m => Math.Abs((m.Created - newestDate).TotalMinutes) < 1).ToArray();
            return measurementsResponse;
        }

        public async Task<GetChartDataResponse> GetLastRecordsAsync(int? itemsToReturn)
        {
            var response = ServiceResponse<GetChartDataResponse>.Success();
            var chartItems = new List<ChartItem>();
            var measurements = await _context.Measurements.ToListAsync();

            if (!measurements.Any())
            {
                return ServiceResponse<GetChartDataResponse>.Error().FieldNotFound("Measurements");
            }

            var measurementsSortedByType = measurements.OrderByDescending(m => m.Created).ToLookup(m => m.Type, m => m);

            foreach (var sortedMeasurements in measurementsSortedByType)
            {
                var itemsToTake = itemsToReturn ?? sortedMeasurements.Count();
                var exactMeasurements = sortedMeasurements.Take(itemsToTake).Reverse();


                var chartItem = new ChartItem();
                chartItem.Title = exactMeasurements.Select(m => m.Type).FirstOrDefault().ToString();
                chartItem.Series = new List<double[]> {exactMeasurements.Select(m => m.Value).ToArray()};
                chartItem.SeriesTitle = new List<string>(exactMeasurements.Select(m => m.Type.ToString()));
                chartItem.CategoryAxis = exactMeasurements.Select(m => m.Created.ToString("dd-MM\nHH:mm")).ToArray();
                chartItems.Add(chartItem);
            }

            if (!chartItems.Any())
            {
                return ServiceResponse<GetChartDataResponse>.Error().FieldNotFound("ChartItems");
            }
            response.ChartItems = chartItems.ToArray();

            return response;
        }

        public async Task<GetChartDataResponse> GetDailyRecordsAsync()
        {
            var response = ServiceResponse<GetChartDataResponse>.Success();
            var chartItems = new List<ChartItem>();
            var measurements = await _context.Measurements.ToListAsync();

            if (!measurements.Any())
            {
                return ServiceResponse<GetChartDataResponse>.Error().FieldNotFound("Measurements");
            }

            var measurementsSortedByType = measurements.OrderByDescending(m => m.Created).ToLookup(m => m.Type, m => m);

            foreach (var sortedMeasurements in measurementsSortedByType)
            {
                var yesterday = DateTimeOffset.Now.AddDays(-1);
                var yesterdayMeasurements = from m in sortedMeasurements
                    where m.Created >= yesterday
                    orderby m.Created descending
                    select m;

                var groupsByHour = yesterdayMeasurements.GroupBy(m => new
                    {m.Created.Date, m.Created.Hour}).Reverse();

                var averageByHour = groupsByHour.Select(g =>
                    new AverageMeasurement {Hour = g.Key.Hour, Date = g.Key.Date, Average = g.Average(m => m.Value)});

                var chartItem = new ChartItem();
                chartItem.Title = yesterdayMeasurements.Select(m => m.Type).FirstOrDefault().ToString();
                chartItem.Series = new List<double[]> {averageByHour.Select(m => m.Average).ToArray()};
                chartItem.SeriesTitle = new List<string>(yesterdayMeasurements.Select(m => m.Type.ToString()));
                chartItem.CategoryAxis = averageByHour.Select(m => $"{m.Date.AddHours(m.Hour):dd.MM\nHH:mm}").ToArray();
                chartItems.Add(chartItem);
            }

            if (!chartItems.Any())
            {
                return ServiceResponse<GetChartDataResponse>.Error().FieldNotFound("ChartItems");
            }
            response.ChartItems = chartItems.ToArray();

            return response;
        }

        public async Task<GetChartDataResponse> GetWeeklyRecordsAsync()
        {
            var response = ServiceResponse<GetChartDataResponse>.Success();
            var chartItems = new List<ChartItem>();
            var measurements = await _context.Measurements.ToListAsync();

            if (!measurements.Any())
            {
                return ServiceResponse<GetChartDataResponse>.Error().FieldNotFound("Measurements");
            }

            var measurementsSortedByType = measurements.OrderByDescending(m => m.Created).ToLookup(m => m.Type, m => m);


            foreach (var sortedMeasurements in measurementsSortedByType)
            {
                var lastWeek = DateTimeOffset.Now.AddDays(-7);
                var lastWeekMeasurements = from m in sortedMeasurements
                    where m.Created >= lastWeek
                    orderby m.Created descending
                    select m;

                var groupsByDay = lastWeekMeasurements.GroupBy(m => new
                    {m.Created.Date}).Reverse();

                var averageByDay = groupsByDay.Select(g =>
                    new AverageMeasurement {Date = g.Key.Date, Average = g.Average(m => m.Value)});

                var chartItem = new ChartItem();
                chartItem.Title = lastWeekMeasurements.Select(m => m.Type).FirstOrDefault().ToString();
                chartItem.Series = new List<double[]> {averageByDay.Select(m => m.Average).ToArray()};
                chartItem.SeriesTitle = new List<string>(lastWeekMeasurements.Select(m => m.Type.ToString()));
                chartItem.CategoryAxis = averageByDay.Select(m => m.Date.ToString("dd.MM")).ToArray();
                chartItems.Add(chartItem);
            }

            if (!chartItems.Any())
            {
                return ServiceResponse<GetChartDataResponse>.Error().FieldNotFound("ChartItems");
            }
            response.ChartItems = chartItems.ToArray();

            return response;
        }

        public async Task ClearDatabase()
        {
            foreach (var measurement in _context.Measurements)
            {
                _context.Measurements.Remove(measurement);
            }
            await _context.SaveChangesAsync();
        }

        public async Task Generate3DaySeries()
        {
            var newMeasurements = RandomMeasurements.GemerateForDays();
            await _context.Measurements.AddRangeAsync(newMeasurements);
            await _context.SaveChangesAsync();
        }

        public async Task GenerateSingleSeries()
        {
            var newMeasurements = RandomMeasurements.GenerateList();
            await _context.Measurements.AddRangeAsync(newMeasurements);
            await _context.SaveChangesAsync();
        }
    }
}
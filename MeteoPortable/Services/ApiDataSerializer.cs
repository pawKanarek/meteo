﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using Newtonsoft.Json;

namespace MeteoPortable.Services
{
    public class ApiDataSerializer : IApiDataSerializer
    {
        public string ToPost(object obj)
        {
            var query = string.Empty;
            if (obj != null)
            {
                query = JsonConvert.SerializeObject(obj);
            }
            return query;
        }

        public string ToGet(object obj)
        {
            if (obj == null)
            {
                return null;
            }

            var properties = new List<PropertyInfo>();
            GetProperties(properties, obj.GetType());
            var strProperties = from p in properties
                where p.GetValue(obj, null) != null
                select GetPropertyNameAndValue(obj, p);

            return string.Join("&", strProperties.ToArray());
        }

        private void GetProperties(List<PropertyInfo> properties, Type obj)
        {
            properties.AddRange(obj.GetTypeInfo().DeclaredProperties);
            if (obj.GetTypeInfo().BaseType != null)
            {
                GetProperties(properties, obj.GetTypeInfo().BaseType);
            }
        }

        private static string GetPropertyNameAndValue(object sourceObject, PropertyInfo property)
        {
            string retString;
            var fieldName = property.Name;

            if (property.PropertyType == typeof(List<string>))
            {
                retString = GetCommaDelimitedString<string>(property, fieldName, sourceObject);
            }
            else if (property.PropertyType == typeof(IList<int>))
            {
                retString = GetCommaDelimitedString<int>(property, fieldName, sourceObject);
            }
            else
            {
                retString = fieldName + "=" + WebUtility.UrlEncode(property.GetValue(sourceObject, null).ToString());
            }
            return retString;
        }

        private static string GetCommaDelimitedString<T>(PropertyInfo property, string fieldName, object sourceObject)
        {
            var list = (List<T>) property.GetValue(sourceObject, null);
            return list != null && list.Any() ? fieldName + "=" + string.Join(",", list.ToArray()) : null;
        }
    }
}
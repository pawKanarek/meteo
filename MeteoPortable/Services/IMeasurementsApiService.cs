﻿using System.Threading.Tasks;
using MeteoPortable.Data;
using MeteoPortable.Infrastructure;

namespace MeteoPortable.Services
{
    public interface IMeasurementsApiService
    {
        Task<ApiResponseWrapper<GetChartDataResponse>> GetLastChartDataAsync(GetLastChartDataArgs args);
        Task<ApiResponseWrapper<GetChartDataResponse>> GetWeekyChartDataAsync(GetChartDataArgs args);
        Task<ApiResponseWrapper<GetChartDataResponse>> GetDailyChartDataAsync(GetChartDataArgs args);
    }
}
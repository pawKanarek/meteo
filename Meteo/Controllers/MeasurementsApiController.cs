﻿using System.Threading.Tasks;
using MeteoPortable.Data;
using MeteoWww.Services;
using Microsoft.AspNetCore.Mvc;

namespace MeteoWww.Controllers
{
    [Route("api/")]
    public class MeasurementsApiController : Controller
    {
        private readonly IMeasurementsService _measurementsService;

        public MeasurementsApiController(IMeasurementsService measurementsService)
        {
            _measurementsService = measurementsService;
        }

        // GET: api/Measurements/GetMeasurements
        [HttpGet("GetMeasurements")]
        public async Task<IActionResult> GetAllMeasurementsAsync()
        {
            var response = await _measurementsService.GetAllMeasurementsAsync();
            return new ObjectResult(response);
        }

        [HttpGet("GetWeeklyChart")]
        public async Task<IActionResult> GetWeeklyChartDataAsync()
        {
            var response = await _measurementsService.GetWeeklyRecordsAsync();
            return new ObjectResult(response);
        }

        [HttpGet("GetDailyChart")]
        public async Task<IActionResult> GetDailyChartDataAsync()
        {
            var response = await _measurementsService.GetDailyRecordsAsync();
            return new ObjectResult(response);
        }

        [HttpGet("GetLastRecordsChart")]
        public async Task<IActionResult> GetLastRecordsAsync([FromQuery] GetLastChartDataArgs args)
        {
            args.Count = args.Count != 0 ? args.Count : 20;
            var response = await _measurementsService.GetLastRecordsAsync(args.Count);
            return new ObjectResult(response);
        }

        // POST api/values
        [HttpPost("InsertMeasurements")]
        public async Task<IActionResult> Post([FromBody] MeasurementInsertBody data)
        {
            //_context.Measurements.AddRange(items);
            //_context.SaveChanges();
            var response = await _measurementsService.InsertMeasurementsAsync(data.Measurements.ToArray());

            return new ObjectResult(response);
        }
    }
}
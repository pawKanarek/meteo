﻿namespace MeteoPortable.Data
{
    public class GetLastChartDataArgs
    {
        public int Count { get; set; }
    }
}
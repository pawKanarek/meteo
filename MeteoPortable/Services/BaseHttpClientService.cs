﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using MeteoPortable.Data;
using MeteoPortable.Infrastructure;
using Newtonsoft.Json;

namespace MeteoPortable.Services
{
    public class BaseHttpClientService : IBaseHttpClientService, IDisposable
    {
        protected readonly JsonSerializerSettings DefaultJsonSerializerSettings;
        protected readonly ILifetimeScope Scope;

        private bool _disposed;
        protected HttpClient HttpClient;

        public BaseHttpClientService(ILifetimeScope scope)
        {
            Scope = scope;

            HttpClient = new HttpClient();
            HttpClient.DefaultRequestHeaders.Accept.Clear();
            HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpClient.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Charset", "UTF-8");
            DefaultJsonSerializerSettings = new JsonSerializerSettings();
        }

        protected async Task<ApiResponseWrapper<TOut>> PerformRequestAsync<TOut>(HttpRequestMessage requestMessage)
            where TOut : ResponseBase
        {
            // Create ApiResponseWrapper and pass information about repeating and also input message
            var resp = new ApiResponseWrapper<TOut>();

            try
            {
                CheckInternetConnection();
                using (var cancellationToken = new CancellationTokenSource())
                {
                    await SendMessageAsync(cancellationToken, resp, requestMessage);
                }
            }
            catch (OperationCanceledException)
            {
                resp.StatusCode = RequestStatusCode.Cancelled;
                resp.StatusErrorMessage = "operationCancelledException";
            }
            catch (ObjectDisposedException)
            {
                resp.StatusCode = RequestStatusCode.InterruptedByUser;
            }
            catch (NoConnectionException)
            {
                resp.StatusCode = RequestStatusCode.NoConnection;
                resp.StatusErrorMessage = "noInternetConnection";
            }
            catch (TimeoutException)
            {
                resp.StatusCode = RequestStatusCode.TimeoutException;
                resp.StatusErrorMessage = "timeoutException";
            }
            catch (WebException)
            {
                resp.StatusCode = RequestStatusCode.WebException;
                resp.StatusErrorMessage = "webException";
            }
            catch (Exception)
            {
            }
            return resp;
        }

        protected virtual void CheckInternetConnection()
        {
        }

        protected async Task SendMessageAsync<TOut>(CancellationTokenSource cancellationToken,
            ApiResponseWrapper<TOut> resp,
            HttpRequestMessage requestMessage)
            where TOut : ResponseBase
        {
            requestMessage.Headers.Add("X-Api-Date-Format", "iso");

            var responseMessage = await HttpClient.SendAsync(requestMessage, cancellationToken.Token);
            if (responseMessage.IsSuccessStatusCode)
            {
                resp.ResponseHeaders = responseMessage.Headers;
                resp.HttpStatusCode = responseMessage.StatusCode;
            }
            else
            {
                resp.StatusCode = RequestStatusCode.HttpError;
                resp.StatusErrorMessage = resp.HttpStatusCode.ToString();
            }

            try
            {
                var str = await responseMessage.Content.ReadAsStringAsync();
                resp.ApiResponse = JsonConvert.DeserializeObject<TOut>(str, DefaultJsonSerializerSettings);
            }
            catch (Exception)
            {
            }
        }

        #region IHttpClientService

        public string ApiBaseUrl { get; private set; } = string.Empty;

        public void SetApiBaseUrl(string apiBaseUrl)
        {
            ApiBaseUrl = apiBaseUrl;
            var baseUri = new Uri(ApiBaseUrl);
            HttpClient.BaseAddress = baseUri;
        }


        public async Task<ApiResponseWrapper<TOut>> DoPostAsync<TArgs, TOut>(ApiRequestData<TArgs> requestData)
            where TOut : ResponseBase
        {
            using (var reqMsg = new HttpRequestMessage(HttpMethod.Post, requestData.TargetUrl))
            {
                var dataSerializer = Scope.Resolve<IApiDataSerializer>();
                using (reqMsg.Content = new StringContent(dataSerializer.ToPost(requestData.ApiRequestArgs),
                    Encoding.UTF8, "application/json"))
                {
                    return await PerformRequestAsync<TOut>(reqMsg);
                }
            }
        }

        public async Task<ApiResponseWrapper<TOut>> DoGetAsync<TArgs, TOut>(ApiRequestData<TArgs> requestData)
            where TOut : ResponseBase
        {
            var dataSerializer = Scope.Resolve<IApiDataSerializer>();
            var reqQueryString = dataSerializer.ToGet(requestData.ApiRequestArgs);
            var url = requestData.TargetUrl;
            if (!string.IsNullOrWhiteSpace(reqQueryString))
            {
                url += (reqQueryString.StartsWith("?") ? "" : "?") + reqQueryString;
            }
            using (var reqMsg = new HttpRequestMessage(HttpMethod.Get, url))
            {
                return await PerformRequestAsync<TOut>(reqMsg);
            }
        }

        public async Task<ApiResponseWrapper<TOut>> DoSendRequestAsync<TOut>(HttpRequestMessage reqMsg)
            where TOut : ResponseBase
        {
            return await PerformRequestAsync<TOut>(reqMsg);
        }

        #endregion


        #region IDisposable

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                Clean();
            }
            _disposed = true;
        }

        protected virtual void Clean()
        {
            HttpClient.Dispose();
        }

        #endregion
    }
}
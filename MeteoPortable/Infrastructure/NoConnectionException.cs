﻿using System;

namespace MeteoPortable.Infrastructure
{
    public class NoConnectionException : Exception
    {
    }
}
﻿using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using MeteoWww.Models;
using MeteoWww.Services;
using Microsoft.AspNetCore.Mvc;

namespace Meteo.Controllers
{
    public class HomeController : Controller
    {
        private readonly IMeasurementsService _measurementsService;
        private readonly IOneSignalService _oneSignalService;

        public HomeController(IMeasurementsService measurementsService, IOneSignalService oneSignalService)
        {
            _measurementsService = measurementsService;
            _oneSignalService = oneSignalService;
        }

        public async Task<IActionResult> Index()
        {
            var viewModel = new HomeModel();
            var response = await _measurementsService.GetNewestMeasurementsAsync();
            if (response.Result.Success)
            {
                viewModel.Measurements = response.Measurements.ToList();
                viewModel.Measurement = viewModel.Measurements.FirstOrDefault();
            }
            return View(viewModel);
        }

        [ValidateAntiForgeryToken]
        public async Task<IActionResult> GenerateSingleSeries()
        {
            await _measurementsService.GenerateSingleSeries();
            return new JsonResult("");
        }

        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ClearDatabase()
        {
            await _measurementsService.ClearDatabase();
            return new JsonResult("");
        }

        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Generate3DaySeries()
        {
            await _measurementsService.Generate3DaySeries();
            return new JsonResult("");
        }

        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SendPush()
        {
            _oneSignalService.SendTemperatureWarning(50);
            return new JsonResult("");
        }


        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}
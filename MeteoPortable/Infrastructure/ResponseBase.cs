﻿using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace MeteoPortable.Infrastructure
{
    [DataContract]
    [JsonObject(MemberSerialization.OptOut)]
    public abstract class ResponseBase
    {
        public ResponseBase()
        {
            Result = new ServiceResult();
        }

        [DataMember]
        public ServiceResult Result { get; set; }

        [DataContract]
        public class ServiceResult
        {
            public ServiceResult()
            {
            }

            public ServiceResult(bool success = false, int code = 0)
            {
                Success = success;
                Code = code;
            }

            [DataMember]
            public bool Success { get; private set; }

            [DataMember]
            public int Code { get; private set; }
        }
    }


    public class ServiceError<T>
        where T : ResponseBase
    {
        private T response;

        public ServiceError(T response)
        {
            this.response = response;
        }

        public static T Create<T>(ServiceError<T> @this, bool success = false, int code = 0)
            where T : ResponseBase
        {
            @this.response.Result = new ResponseBase.ServiceResult(success, code);
            return @this.response;
        }
    }

    public static class ServiceResponse<T>
        where T : ResponseBase, new()
    {
        public static T Success(string message = null)
        {
            return new T().Success(message);
        }

        public static T Error(string message)
        {
            return new T().Error(message);
        }

        public static ServiceError<T> Error()
        {
            return new T().Error();
        }
    }

    public static class ServiceResponseExtensions
    {
        public static ServiceError<T> Error<T>(this T @this) where T : ResponseBase
        {
            return new ServiceError<T>(@this);
        }

        public static T Success<T>(this T @this, string message = null) where T : ResponseBase
        {
            @this.Result = new ResponseBase.ServiceResult(true, ServiceErrors.ErrorCodes.NoError);
            return @this;
        }

        public static T Error<T>(this T @this, string message) where T : ResponseBase
        {
            return ServiceError<T>.Create(@this.Error(), false, ServiceErrors.ErrorCodes.Error);
        }
    }

    public static class ServiceErrors
    {
        public static T ServiceContractBroken<T>(this ServiceError<T> @this, string message) where T : ResponseBase
        {
            return ServiceError<T>.Create(@this, false, ErrorCodes.ServiceContractBroken);
        }

        public static T FieldNotFound<T>(this ServiceError<T> @this, string message) where T : ResponseBase
        {
            return ServiceError<T>.Create(@this, false, ErrorCodes.FieldNotFound);
        }

        public static class ErrorCodes
        {
            public const int NoError = 0;
            public const int FieldNotFound = 1;
            public const int ServiceContractBroken = 2;
            public const int Error = 3;
        }
    }
}
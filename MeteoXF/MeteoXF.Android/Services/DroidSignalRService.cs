﻿using System;
using System.Threading.Tasks;
using Android.App;
using Java.Lang;
using MeteoXF.Droid.Infrastructure;
using MeteoXF.Services;
using Microsoft.AspNet.SignalR.Client;
using Xamarin.Forms;

namespace MeteoXF.Droid.Services
{
    public class DroidSignalRService : ISignalRService
    {
        private HubConnection _hubConnection;
        public event EventHandler OnReceviedEvent;

        public DroidSignalRService()
        {
            _hubConnection = new HubConnection("http://meteowww2017.azurewebsites.net/");
            var chatHubProxy = _hubConnection.CreateHubProxy("SignalRHub");
            chatHubProxy.On<string>("Send", message =>
            {
                if (OnReceviedEvent != null)
                {
                    OnReceviedEvent(this, new EventArgs() { });
                }
            });
           
        }

        public async Task InitAsync()
        {
            try
            {
                await _hubConnection.Start();
            }
            catch (System.Exception)
            {
               //sometimes there can be random exception on that plugin. 
            }
            
        }
    }
}
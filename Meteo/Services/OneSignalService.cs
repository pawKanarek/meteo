﻿using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;

namespace MeteoWww.Services
{
    public class OneSignalService : IOneSignalService
    {
        public void SendTemperatureWarning(int temperature)
        {
            //Using default implemntatnion from onesignal example
            var request = WebRequest.Create("https://onesignal.com/api/v1/notifications") as HttpWebRequest;

            request.KeepAlive = true;
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";

            request.Headers.Add("authorization", "Basic YWJhNzJjMDEtYTExOS00NjNkLTljNGYtMDQxMDk2N2ZhMDc4");

            var byteArray = Encoding.UTF8.GetBytes("{"
                                                   + "\"app_id\": \"c0dcbf83-4f5d-412b-89e7-acecd31a9f88\","
                                                   + "\"headings\": {\"en\": \"Uwaga!\"},"
                                                   + "\"contents\": {\"en\": \"Za wyskoka temperatura! Możliwy pożar! " +
                                                   temperature + "C \"},"
                                                   + "\"included_segments\": [\"All\"]}");

            string responseContent = null;

            try
            {
                using (var writer = request.GetRequestStream())
                {
                    writer.Write(byteArray, 0, byteArray.Length);
                }

                using (var response = request.GetResponse() as HttpWebResponse)
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        responseContent = reader.ReadToEnd();
                    }
                }
            }
            catch (WebException ex)
            {
                Debug.WriteLine(ex.Message);
                Debug.WriteLine(new StreamReader(ex.Response.GetResponseStream()).ReadToEnd());
            }
            Debug.WriteLine(responseContent);
        }
    }
}
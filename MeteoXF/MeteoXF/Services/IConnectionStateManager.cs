﻿using System;
using System.Threading.Tasks;

namespace MeteoXF.Services
{
    public interface IConnectionStateManager
    {
        bool IsConnectedToInternet { get; }
        double ShowAlertIntervalSeconds { get; set; }
        IObservable<bool> WhenConnectivityChanged { get; }
        Task InitializeAsync();
        Task CheckInternetConnectionAsync();
        Task CheckNetworkAlertVisibilityAsync();
    }
}
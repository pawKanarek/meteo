﻿using System;
using System.Linq;
using Meteo.Models;
using MeteoPortable.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace MeteoWww.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new MeteoContext(serviceProvider.GetRequiredService<DbContextOptions<MeteoContext>>()))
            {
                if (context.Measurements.Any())
                {
                    return; // DB has been seeded
                }

                var mesasurForWeek = RandomMeasurements.GemerateForDays();
                context.Measurements.AddRange(mesasurForWeek);
                context.SaveChanges();
            }
        }
    }
}
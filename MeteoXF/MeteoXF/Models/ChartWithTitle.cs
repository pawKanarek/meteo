﻿using Microcharts;

namespace MeteoXF.Models
{
    public class ChartWithTitle
    {
        public LineChart LineChart { get; set; }

        public string Title { get; set; }
    }
}
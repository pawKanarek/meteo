﻿using System.Linq;
using System.Threading.Tasks;
using MeteoWww.Models;
using MeteoWww.Services;
using Microsoft.AspNetCore.Mvc;

namespace Meteo.Controllers
{
    public class MeasurementsController : Controller
    {
        private readonly IMeasurementsService _measurementsService;

        public MeasurementsController(IMeasurementsService measurementsService)
        {
            _measurementsService = measurementsService;
        }

        // GET: Measurements
        public async Task<IActionResult> Last20()
        {
            var response = await _measurementsService.GetLastRecordsAsync(20);
            if (response.Result.Success)
            {
                var measurementsModel = new MeasurementsModel();
                measurementsModel.ChartItems = response.ChartItems.ToList();
                return View(measurementsModel);
            }
            return NotFound();
        }

        public async Task<IActionResult> Weekly()
        {
            var response = await _measurementsService.GetWeeklyRecordsAsync();
            if (response.Result.Success)
            {
                var measurementsModel = new MeasurementsModel();
                measurementsModel.ChartItems = response.ChartItems.ToList();
                return View(measurementsModel);
            }
            return NotFound();
        }

        public async Task<IActionResult> Daily()
        {
            var response = await _measurementsService.GetDailyRecordsAsync();
            if (response.Result.Success)
            {
                var measurementsModel = new MeasurementsModel();
                measurementsModel.ChartItems = response.ChartItems.ToList();
                return View(measurementsModel);
            }
            return NotFound();
        }
    }
}
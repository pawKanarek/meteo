﻿using System.Threading.Tasks;

namespace MeteoXF.Services
{
    public interface IAlertService
    {
        Task<bool> DisplayAlertAsync(string title, string message, string ok, string cancel);

        Task DisplayAlertAsync(string title, string message, string cancel);
    }
}
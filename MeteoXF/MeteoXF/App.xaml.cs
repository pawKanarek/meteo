﻿using Autofac;
using Com.OneSignal;
using MeteoPortable.Services;
using MeteoXF.Services;
using MeteoXF.Services.Implementations;
using MeteoXF.Views;
using Prism.Autofac;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]

namespace MeteoXF
{
    public partial class App : PrismApplication
    {
        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App() : this(null)
        {
        }

        public App(IPlatformInitializer initializer)
            : base(initializer)
        {
            Current.MainPage = new ContentPage
            {
                Content = new ActivityIndicator
                {
                    IsRunning = true
                }
            };
        }

        protected override async void OnInitialized()
        {
            InitializeComponent();
            using (var scope = Container.BeginLifetimeScope())
            {
                var connectionStateManager = scope.Resolve<IConnectionStateManager>();
                var signalRService = scope.Resolve<ISignalRService>();
                await signalRService.InitAsync();
                await connectionStateManager.InitializeAsync();
            }
            await NavigationService.NavigateAsync("HomeMasterDetail/NavigationPage/Last20Page");
            OneSignal.Current.StartInit("c0dcbf83-4f5d-412b-89e7-acecd31a9f88").EndInit();
        }

        protected override void RegisterTypes()
        {
            RegisterPages();
            RegisterServices();
        }

        private void RegisterServices()
        {
            Builder.RegisterType<ApiDataSerializer>().As<IApiDataSerializer>().InstancePerDependency();
            Builder.RegisterType<ConnectionStateManager>().As<IConnectionStateManager>().SingleInstance();
            Builder.RegisterType<ConnectivityService>().As<IConnectivityService>().InstancePerLifetimeScope();
            Builder.RegisterType<MobileHttpClientService>().As<IBaseHttpClientService>().InstancePerDependency();
            Builder.RegisterType<MeasurementsApiService>().As<IMeasurementsApiService>().InstancePerLifetimeScope();
        }

        private void RegisterPages()
        {
            Builder.RegisterTypeForNavigation<NavigationPage>();
            Builder.RegisterTypeForNavigation<DailyPage>();
            Builder.RegisterTypeForNavigation<WeeklyPage>();
            Builder.RegisterTypeForNavigation<Last20Page>();
            Builder.RegisterTypeForNavigation<HomeMasterDetail>();
        }
    }
}
﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MeteoXF.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomeMasterDetail : MasterDetailPage
    {
        public HomeMasterDetail()
        {
            InitializeComponent();
        }

        protected override bool OnBackButtonPressed()
        {
            return false;
        }
    }
}
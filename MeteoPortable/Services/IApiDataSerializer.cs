﻿namespace MeteoPortable.Services
{
    public interface IApiDataSerializer
    {
        string ToPost(object obj);

        string ToGet(object obj);
    }
}
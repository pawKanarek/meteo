﻿using System;
using Android.Runtime;
using Autofac;
using MeteoXF.Droid.Services;
using MeteoXF.Services;
using Prism.Autofac;

namespace MeteoXF.Droid
{
    public class AndroidInitializer : IPlatformInitializer
    {
        public AndroidInitializer()
        {
            AndroidEnvironment.UnhandledExceptionRaiser += AndroidEnvironment_UnhandledExceptionRaiser;
        }

        public void RegisterTypes(ContainerBuilder builder)
        {
            builder.RegisterType<DroidAlertService>().As<IAlertService>().InstancePerLifetimeScope();
            builder.RegisterType<DroidSignalRService>().As<ISignalRService>().SingleInstance();
            // Register any platform specific implementations
        }

        protected void AndroidEnvironment_UnhandledExceptionRaiser(object sender, RaiseThrowableEventArgs e)
        {
            Console.WriteLine("=== MeteoXF.Droid Exception: " + e.Exception.Message);
            Console.WriteLine("=== MeteoXF.Droid StackTrace: " + e.Exception.StackTrace);
        }
    }
}
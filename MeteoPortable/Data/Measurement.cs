﻿using System;
using MeteoPortable.Infrastructure;

namespace MeteoPortable.Data
{
    public class Measurement
    {
        public int ID { get; set; }
        public DateTimeOffset Created { get; set; }
        public DateTimeOffset Recevied { get; set; }
        public string Title { get; set; }
        public double Value { get; set; }
        public MeasurementType Type { get; set; }
    }
}
﻿using System;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;

namespace MeteoXF.Services
{
    public class ConnectionStateManager : IConnectionStateManager, IDisposable
    {
        protected readonly IAlertService alertService;
        protected readonly IDisposable connectivityChangedSubscription;
        protected readonly IConnectivityService connectivityService;
        protected IDisposable alertTimerElapsedSubscription;
        protected bool isAlertVisible;
        private bool isDisposed;
        private bool isInitialized;

        public ConnectionStateManager(IAlertService alertService, IConnectivityService connectivityService)
        {
            this.alertService = alertService;
            this.connectivityService = connectivityService;
            connectivityChangedSubscription = WhenConnectivityChanged.Subscribe(async x =>
            {
                await CheckInternetConnectionAsync();
            });
        }

        public async Task InitializeAsync()
        {
            if (isInitialized)
            {
                return;
            }

            await CheckInternetConnectionAsync();
            isInitialized = true;
            alertTimerElapsedSubscription = WhenAlertTimerElapsed.Subscribe(async x =>
            {
                await CheckNetworkAlertVisibilityAsync();
            });
        }

        public async Task CheckInternetConnectionAsync()
        {
            IsConnectedToInternet = connectivityService.IsNetworkConnected &&
                                    await connectivityService.IsInternetAvailable("google.com");
            if (!IsConnectedToInternet)
            {
                await CheckNetworkAlertVisibilityAsync();
            }
        }

        public async Task CheckNetworkAlertVisibilityAsync()
        {
            if (IsConnectedToInternet || isAlertVisible || !isInitialized)
            {
                return;
            }

            if (!isAlertVisible)
            {
                isAlertVisible = true;
                await alertService.DisplayAlertAsync(string.Empty, "noInternetConnection", "tryAgain");

                isAlertVisible = false;
            }
        }

        #region Properties

        public bool IsConnectedToInternet { get; private set; }
        public double ShowAlertIntervalSeconds { get; set; } = 7;

        #endregion Properties

        #region IObservable

        public IObservable<long> WhenAlertTimerElapsed =>
            Observable.Interval(TimeSpan.FromSeconds(ShowAlertIntervalSeconds));

        public IObservable<bool> WhenConnectivityChanged
        {
            get
            {
                return Observable.FromEventPattern<ConnectivityChangedEventHandler, ConnectivityChangedEventArgs>(
                        h => CrossConnectivity.Current.ConnectivityChanged += h,
                        h => CrossConnectivity.Current.ConnectivityChanged -= h)
                    .Select(x => x.EventArgs.IsConnected);
            }
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (isDisposed)
            {
                return;
            }

            if (disposing)
            {
                alertTimerElapsedSubscription.Dispose();
                connectivityChangedSubscription.Dispose();
            }

            isDisposed = true;
        }

        #endregion
    }
}
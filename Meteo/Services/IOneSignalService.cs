﻿namespace MeteoWww.Services
{
    public interface IOneSignalService
    {
        void SendTemperatureWarning(int temp);
    }
}
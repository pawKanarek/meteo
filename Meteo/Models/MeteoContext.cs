﻿using MeteoPortable.Data;
using Microsoft.EntityFrameworkCore;

namespace Meteo.Models
{
    public class MeteoContext : DbContext
    {
        public MeteoContext(DbContextOptions<MeteoContext> options)
            : base(options)
        {
        }

        public DbSet<Measurement> Measurements { get; set; }
    }
}
﻿using System.Net.Http;
using System.Net.Http.Headers;
using Autofac;
using MeteoPortable.Infrastructure;
using MeteoPortable.Services;
using ModernHttpClient;

namespace MeteoXF.Services.Implementations
{
    public class MobileHttpClientService : BaseHttpClientService
    {
        private readonly IConnectionStateManager _connectionStateManager;

        public MobileHttpClientService(IConnectionStateManager connectionStateManager, ILifetimeScope scope) :
            base(scope)
        {
            _connectionStateManager = connectionStateManager;
            HttpClient = new HttpClient(new NativeMessageHandler());
            HttpClient.DefaultRequestHeaders.Accept.Clear();
            HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpClient.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Charset", "UTF-8");
        }

        protected override void CheckInternetConnection()
        {
            if (!_connectionStateManager.IsConnectedToInternet)
            {
                throw new NoConnectionException();
            }
        }
    }
}
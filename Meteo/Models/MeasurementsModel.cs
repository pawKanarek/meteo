﻿using System.Collections.Generic;
using MeteoPortable.Data;

namespace MeteoWww.Models
{
    public class MeasurementsModel
    {
        public List<ChartItem> ChartItems { get; set; } = new List<ChartItem>();
        public Measurement Measurement { get; set; }
    }
}
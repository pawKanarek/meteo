﻿using System;
using System.Collections.Generic;
using System.Linq;
using MeteoPortable.Data;

namespace MeteoPortable.Infrastructure
{
    public static class RandomMeasurements
    {
        private static readonly Dictionary<MeasurementType, KeyValuePair<int, int>> DefaultRanges =
            new Dictionary<MeasurementType, KeyValuePair<int, int>>
            {
                {MeasurementType.TemperatueInside, new KeyValuePair<int, int>(15, 25)},
                {MeasurementType.TemperatureOutside, new KeyValuePair<int, int>(0, 30)},
                {MeasurementType.Humidity, new KeyValuePair<int, int>(0, 40)},
                {MeasurementType.Smog, new KeyValuePair<int, int>(0, 400)},
                {MeasurementType.Pressure, new KeyValuePair<int, int>(950, 1050)}
            };

        private static readonly List<Measurement> DefaultData = new List<Measurement>
        {
            new Measurement
            {
                Title = "Temperatura w domu",
                Created = DateTime.Parse("2017-1-11"),
                Recevied = DateTime.Parse("2017-1-11"),
                Type = MeasurementType.TemperatueInside,
                Value = 10
            },
            new Measurement
            {
                Title = "Temperatura na zewnątrz",
                Created = DateTime.Parse("2017-1-11"),
                Recevied = DateTime.Parse("2017-1-11"),
                Type = MeasurementType.TemperatureOutside,
                Value = 0
            },
            new Measurement
            {
                Title = "Wilgotność",
                Created = DateTime.Parse("2017-1-11"),
                Recevied = DateTime.Parse("2017-1-11"),
                Type = MeasurementType.Humidity,
                Value = 20
            },
            new Measurement
            {
                Title = "Ciśnienie",
                Created = DateTime.Parse("2017-1-11"),
                Recevied = DateTime.Parse("2017-1-11"),
                Type = MeasurementType.Pressure,
                Value = 1021
            },
            new Measurement
            {
                Title = "Smog PM 2.5",
                Created = DateTime.Parse("2017-1-11"),
                Recevied = DateTime.Parse("2017-1-11"),
                Type = MeasurementType.Smog,
                Value = 50
            }
        };

        public static List<Measurement> GenerateList()
        {
            var measurements = new List<Measurement>();

            foreach (var measurement in DefaultData)
            {
                var defaultValues = DefaultRanges[measurement.Type];
                var random = new Random();
                var newMeasurement = new Measurement
                {
                    Value = random.Next(defaultValues.Key, defaultValues.Value),
                    Recevied = DateTimeOffset.Now,
                    Created = DateTimeOffset.Now,
                    Type = measurement.Type,
                    Title = measurement.Title
                };
                measurements.Add(newMeasurement);
            }

            return measurements;
        }

        public static List<Measurement> GemerateForDays(int days = 3)
        {
            var measurements = new List<Measurement>();
            var iteratorDate = DateTimeOffset.Now.AddDays(-days);
            var random = new Random();

            while (iteratorDate <= DateTimeOffset.Now)
            {
                iteratorDate = iteratorDate.AddMinutes(5);
                var currentDate = iteratorDate;
                measurements.AddRange(from measurement in DefaultData
                    let defaultValues = DefaultRanges[measurement.Type]
                    select new Measurement
                    {
                        Value = random.Next(defaultValues.Key, defaultValues.Value),
                        Recevied = currentDate,
                        Created = currentDate.AddMinutes(-1),
                        Type = measurement.Type,
                        Title = measurement.Title
                    });
            }
            return measurements;
        }
    }
}
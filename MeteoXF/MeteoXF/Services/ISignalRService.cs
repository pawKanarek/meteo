﻿using System;
using System.Threading.Tasks;

namespace MeteoXF.Services
{
    public interface ISignalRService
    {
        event EventHandler OnReceviedEvent;

        Task InitAsync();
    }
}
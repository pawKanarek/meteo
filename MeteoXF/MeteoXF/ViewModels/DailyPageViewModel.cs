﻿using System.Threading.Tasks;
using MeteoPortable.Data;
using MeteoPortable.Services;
using MeteoXF.Services;
using Prism.Navigation;

namespace MeteoXF.ViewModels
{
    public class DailyPageViewModel : ViewModelBase
    {
        private readonly IMeasurementsApiService _measurementsApiService;
        private readonly INavigationService _navigationService;

        public DailyPageViewModel(INavigationService navigationService, IMeasurementsApiService measurementsApiService, ISignalRService signalRService, IAlertService alertService)
            : base(navigationService, signalRService, alertService)
        {
            _navigationService = navigationService;
            _measurementsApiService = measurementsApiService;

            Title = "Ostatni dzień";

            Task.Run(async () => { await FillDataAsync(); });
        }

        public async Task FillDataAsync()
        {
            IsDataLoading = true;
            IsDataLoaded = false;
            var response = await _measurementsApiService.GetDailyChartDataAsync(new GetChartDataArgs());
            IsDataLoading = false;

            if (response.ApiResponse.Result.Success)
            {
                IsDataLoaded = true;
                CreateChart(response.ApiResponse.ChartItems);
            }
        }
    }
}
﻿using System.Collections.Generic;
using MeteoPortable.Data;

namespace MeteoWww.Models
{
    public class HomeModel
    {
        public List<Measurement> Measurements { get; set; } = new List<Measurement>();
        public Measurement Measurement { get; set; }
    }
}
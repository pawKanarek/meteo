﻿using System;

namespace MeteoPortable.Data
{
    public class AverageMeasurement
    {
        public int Hour { get; set; }
        public DateTimeOffset Date { get; set; }
        public double Average { get; set; }
    }
}
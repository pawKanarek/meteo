﻿namespace MeteoPortable.Data
{
    public class ApiRequestData<TArgs>
    {
        public TArgs ApiRequestArgs { get; set; }
        public string TargetUrl { get; set; }
    }
}
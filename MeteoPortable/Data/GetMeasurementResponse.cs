﻿using MeteoPortable.Infrastructure;

namespace MeteoPortable.Data
{
    public class GetMeasurementResponse : ResponseBase
    {
        public Measurement Measurement { get; set; }
    }
}
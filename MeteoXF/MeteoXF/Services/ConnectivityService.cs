﻿using System.Threading.Tasks;
using Plugin.Connectivity;

namespace MeteoXF.Services
{
    public class ConnectivityService : IConnectivityService
    {
        public bool IsNetworkConnected => CrossConnectivity.Current.IsConnected;

        public async Task<bool> IsInternetAvailable(string hostToCheck)
        {
            return await CrossConnectivity.Current.IsRemoteReachable(hostToCheck);
        }
    }
}
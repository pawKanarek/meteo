﻿using System.Threading.Tasks;
using Android.App;
using Java.Lang;
using MeteoXF.Droid.Infrastructure;
using MeteoXF.Services;
using Xamarin.Forms;

namespace MeteoXF.Droid.Services
{
    public class DroidAlertService : IAlertService
    {
        public async Task DisplayAlertAsync(string title, string message, string cancel)
        {
            var tcs = new TaskCompletionSource<bool>();
            var activity = (Activity) Forms.Context;
            var ft = activity.FragmentManager.BeginTransaction();
            if (ft.IsAddToBackStackAllowed)
            {
                ft.AddToBackStack(null);
            }
            var alert = AlertDialogFragment.NewInstance(null);
            alert.AlertDialogModel = new AlertDialogModel
            {
                Title = title,
                Message = message,
                NegativeButtonText = cancel,
                TaskCompletionSource = tcs
            };

            activity.RunOnUiThread(() =>
            {
                try
                {
                    alert.Show(ft, "dialog");
                }
                catch (IllegalStateException)
                {
                    // There's no way to avoid getting this if saveInstanceState has already been called.
                }
            });

            await alert.AlertDialogModel.TaskCompletionSource.Task;

            activity.RunOnUiThread(() => { alert.DismissAllowingStateLoss(); });
        }

        public async Task<bool> DisplayAlertAsync(string title, string message, string ok, string cancel)
        {
            var result = false;
            var activity = (Activity) Forms.Context;

            var tcs = new TaskCompletionSource<bool>();
            var ft = activity.FragmentManager.BeginTransaction();
            if (ft.IsAddToBackStackAllowed)
            {
                ft.AddToBackStack(null);
            }
            var alert = AlertDialogFragment.NewInstance(null);
            alert.AlertDialogModel = new AlertDialogModel
            {
                Title = title,
                Message = message,
                PositiveButtonText = ok,
                NegativeButtonText = cancel,
                TaskCompletionSource = tcs
            };

            activity.RunOnUiThread(() =>
            {
                try
                {
                    alert.Show(ft, "dialog");
                }
                catch (IllegalStateException)
                {
                    // There's no way to avoid getting this if saveInstanceState has already been called.
                }
            });

            result = await alert.AlertDialogModel.TaskCompletionSource.Task;
            activity.RunOnUiThread(() =>
            {
                try
                {
                    alert.DismissAllowingStateLoss();
                }
                catch (IllegalStateException)
                {
                    // There's no way to avoid getting this if saveInstanceState has already been called.
                }
            });

            return result;
        }
    }
}
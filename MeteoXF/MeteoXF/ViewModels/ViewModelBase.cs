﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using MeteoPortable.Data;
using MeteoXF.Models;
using MeteoXF.Services;
using Microcharts;
using Prism.Mvvm;
using Prism.Navigation;

namespace MeteoXF.ViewModels
{
    public class ViewModelBase : BindableBase, INavigationAware, IDestructible
    {
        private string _title;
        protected ISignalRService _signalRService;
        private readonly IAlertService _alertService;

        public ViewModelBase(INavigationService navigationService, ISignalRService signalRService, IAlertService alertService)
        {
            NavigationService = navigationService;
            _signalRService = signalRService;_alertService = alertService;
            

            _signalRService.OnReceviedEvent += _signalRService_OnReceviedEvent;
        }

        private async void _signalRService_OnReceviedEvent(object sender, System.EventArgs e)
        {
            await FillDataAsync();
        }

        protected INavigationService NavigationService { get; private set; }

        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }


        public virtual void Destroy()
        {
        }

        public virtual void OnNavigatedFrom(NavigationParameters parameters)
        {
        }

        public virtual void OnNavigatedTo(NavigationParameters parameters)
        {
        }

        public virtual void OnNavigatingTo(NavigationParameters parameters)
        {
        }

        public virtual async Task FillDataAsync()
        {
        }

        public void CreateChart(ChartItem[] chartItems)
        {
            var charts = new List<ChartWithTitle>();
            foreach (var singleChartItem in chartItems)
            {
                var entries = new List<Entry>();
                var singleSerie = singleChartItem?.Series?.FirstOrDefault();
                if (singleSerie == null)
                {
                    return;
                }

                for (var i = 0; i < singleSerie.Count(); i++)
                {
                    entries.Add(new Entry((float) singleSerie[i])
                    {
                        Label = singleChartItem.CategoryAxis[i],
                        ValueLabel = singleSerie[i].ToString()
                    });
                }
                var max = singleSerie.ToList().Max();
                var min = singleSerie.ToList().Min();
                var diff = max - min;
                var range = diff * 0.25;

                charts.Add(new ChartWithTitle
                {
                    LineChart = new LineChart
                    {
                        Entries = entries,
                        MinValue = (float) (min - range),
                        MaxValue = (float) (max + range)
                    },
                    Title = singleChartItem.Title
                });
            }
            ItemsSource = new ObservableCollection<ChartWithTitle>(charts);
        }

        #region BindableProperties

        private bool _isDataLoading;

        public bool IsDataLoading
        {
            get => _isDataLoading;
            set => SetProperty(ref _isDataLoading, value);
        }

        private bool _isDataLoaded;

        public bool IsDataLoaded
        {
            get => _isDataLoaded;
            set => SetProperty(ref _isDataLoaded, value);
        }

        private ObservableCollection<ChartWithTitle> _itemsSource;

        public ObservableCollection<ChartWithTitle> ItemsSource
        {
            get => _itemsSource;
            set => SetProperty(ref _itemsSource, value);
        }

        #endregion
    }
}
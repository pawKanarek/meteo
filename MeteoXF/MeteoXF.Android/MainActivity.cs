﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Microsoft.AspNet.SignalR.Client;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

namespace MeteoXF.Droid
{
    [Activity(Label = "MeteoXF", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true,
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            Forms.Init(this, bundle);
            LoadApplication(new App(new AndroidInitializer()));
        }
    }
}
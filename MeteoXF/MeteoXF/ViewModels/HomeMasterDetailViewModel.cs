﻿using System.Diagnostics;
using System.Threading.Tasks;
using MeteoPortable.Data;
using MeteoPortable.Services;
using MeteoXF.Services;
using Microcharts;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using SkiaSharp;

namespace MeteoXF.ViewModels
{
    public class HomeMasterDetailViewModel : ViewModelBase
    {
        private readonly IMeasurementsApiService _measurementsApiService;
        private readonly INavigationService _navigationService;


        public HomeMasterDetailViewModel(INavigationService navigationService, ISignalRService signalRService, IAlertService alertService,
            IMeasurementsApiService measurementsApiService)
            : base(navigationService, signalRService, alertService)
        {
            _navigationService = navigationService;
            _measurementsApiService = measurementsApiService;

            SetCommands();

            Title = "Home Page";
            Last20Chart = CreateChart();

            Task.Run(async () => { await FillDataAsync(); });
        }

        public DelegateCommand<string> NavigateCommand { get; set; }

        private void SetCommands()
        {
            NavigateToSpeakPageCommand = new DelegateCommand(NavigateToSpeakPage);
            DisplayAlertCommand = new DelegateCommand(DisplayAlert);
            DisplayActionSheetCommand = new DelegateCommand(DisplayActionSheet);
            NavigateCommand = new DelegateCommand<string>(Navigate);
        }

        private void Navigate(string name)
        {
            _navigationService.NavigateAsync(name);
        }

        public async Task FillDataAsync()
        {
            var response = await _measurementsApiService.GetLastChartDataAsync(new GetLastChartDataArgs {Count = 20});

            if (response.ApiResponse.Result.Success)
            {
            }
        }

        private LineChart CreateChart()
        {
            var entries = new[]
            {
                new Entry(200)
                {
                    Label = "January",
                    ValueLabel = "200",
                    Color = SKColor.Parse("#266489")
                },
                new Entry(400)
                {
                    Label = "February",
                    ValueLabel = "400",
                    Color = SKColor.Parse("#68B9C0")
                },
                new Entry(-100)
                {
                    Label = "March",
                    ValueLabel = "-100",
                    Color = SKColor.Parse("#90D585")
                }
            };

            return new LineChart {Entries = entries};
        }


        private async void DisplayActionSheet()
        {
            var selectAAction = ActionSheetButton.CreateButton("Select A", () => { Debug.WriteLine("Select A"); });
            var selectBAction = ActionSheetButton.CreateButton("Select B", () => { Debug.WriteLine("Select B"); });
            var selectCAction = ActionSheetButton.CreateButton("Select C", () => { Debug.WriteLine("Select C"); });
            var cancelAction = ActionSheetButton.CreateCancelButton("Cancel", () => { Debug.WriteLine("Cancel"); });
        }

        private async void DisplayAlert()
        {
        }

        private async void NavigateToSpeakPage()
        {
            await _navigationService.NavigateAsync("SpeakPage");
        }

        #region BindableProperties

        private LineChart _last20Chart;

        public LineChart Last20Chart
        {
            get => _last20Chart;
            set => SetProperty(ref _last20Chart, value);
        }

        #endregion

        #region Commands

        public DelegateCommand NavigateToSpeakPageCommand { get; private set; }
        public DelegateCommand DisplayAlertCommand { get; private set; }
        public DelegateCommand DisplayActionSheetCommand { get; private set; }

        #endregion
    }
}